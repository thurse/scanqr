# scanqr

A simple bash script using zbarcam to automate QR code handling via webcam.

## Prerequisites / Setup

Install [zbar](http://zbar.sourceforge.net/) on your system which provides
the `zbarcam` binary.

Also make sure you have `xdg-open`, `xclip` and `notify-send` available.

Download and `chmod u+x ./scanqr`

## Usage

**WARNING:** This script accesses your webcam without warning you explicitly AND
automatically opens websites in your browser.
If you don't like this, feel free to an an additional `notify-send` or some other
kind of check dialogue.

- Type `./scanqr`. If you have a webcam status indicator light it should light up, which
  means that you can present the QR code to the camera.
- Based on the content of the QR code the script invokes specific handlers:

    - If the QR-Code contains a URL it accesses it via your default browser.
    - If it contains geo information (lat/lon) it searches the location with Google Maps.
    - If it contains WIFI credentials they're getting displayed in your desktop
      notification system
    - Any other case: Copy content to your system clipboard.
    
- scanqr (and zbarcam) will exit after any successful QR scan, no matter whether
  the content could be processed or not
